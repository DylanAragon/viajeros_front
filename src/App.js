import React, { useState, useEffect } from "react";
import './App.css';
import styled from 'styled-components';
import 'bootstrap/dist/css/bootstrap.min.css';
import StarsRating from 'stars-rating'
import prueba from './prueba.json'
import { Button, Form, FormGroup, Label, FormText, Col, Row,InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  Input,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroupText } from 'reactstrap';

const ContenedorUsuarios = styled.div`

background-color:beige;
text-align:center;
padding: 30px;

`

const PerfilUsuario = styled.div`

border:1px solid #3D3D3D;
background-color:#64FBBE;
border-radius:20px;
color:#000000;
margin:5px;
padding:10px;
text-align:center;

`

const ImagenPerfil = styled.img`

width: 150px;
height: 150px;
border: 1px solid black;
border-radius: 200px;

`

const FondoRegistro = styled.div`

background-color: #CD76FF;
border: 1px solid red;
text-align:center;
padding:10px;
margin-top: 100px;

`

const ComponenteForm = styled.div`

text-align:center;

`
const DivExterior = styled.div`

width: 700px;
height: 500px;
background-color:red;

`


function App() {
  /*
    const url = "https://pokeapi.co/api/v2/pokemon?limit=100&offset=200"
    const [viajeros, setViajeros] = useState([]);
*/
  /*
  
  useEffect(() => {
    fetch(url)
      .then(data => data.json())
      .then(datos => setViajeros(datos))
      .catch(err => console.log(err));
  },[])
*/
  /*
  useEffect(()=>{
    Controller.getbyViajeros()
     .then(data => setViajeros(data))
     .catch(err => setError(err.message));
   }, [])
   */

  const ratingChanged = (newRating) => {
    console.log(newRating)
  }

  // esto es para crear cada viajero
  /*
  const filas = prueba.map((el) => (
    <div className="col-lg-3 col-sm-12">
      <PerfilUsuario>
        <ImagenPerfil src="https://img.icons8.com/clouds/452/pokemon-go.png" />
        <h4>{el.name}</h4>
        <StarsRating
          count={5}
          value={4}
          size={24}
          color2={'#ffd700'} />
        <div><i class="fa fa-calendar" aria-hidden="true"></i>__el.fecha__<i class="fa fa-calendar" aria-hidden="true"></i></div>
        <div><i class="fa fa-money" aria-hidden="true"></i>______€€€______<i class="fa fa-money" aria-hidden="true"></i></div>
      </PerfilUsuario >
    </div>
  ));

  <div className="container">
        <div className="row">
          {filas}
        </div>
        </div>

  */
  /* Esto es el formulario para registrarse en la pagina web
 
         <FondoRegistro className="container">
           <Form>
            <Row>
              <ComponenteForm className="col-lg-6">
             <Label for="idnombre">Nombre</Label>
             <Input type="text" name="nombre" id="idnombre" />
              </ComponenteForm>
              <ComponenteForm className="col-lg-6">
             <Label for="idapellidos">Apellidos</Label>
             <Input type="text" name="apellidos" id="idapellidos" />
              </ComponenteForm>
            </Row>
           <Row>
           <ComponenteForm className="col-lg-6">
             <Label for="idemail">Email</Label>
             <Input type="email" name="email" id="idemail" />
           </ComponenteForm>
           <ComponenteForm className="col-lg-6">
             <Label for="password">Password</Label>
             <Input type="password" name="password" id="password" />
           </ComponenteForm>
           </Row>
           <Row>
           <ComponenteForm className="col-lg-6">
               <Label for="ididentificacion">Identificacion</Label>
             <Row>
               <Col className="col-4">
                 <Input type="select" name="nombre" id="ididentificacion">
                   <option>DNI</option>
                   <option>Passaporte</option>
                 </Input></Col>
               <Col className="col-8">
                 <Input type="text" name="nombre" id="ididentificacion" />
               </Col>
             </Row>
           </ComponenteForm>
           <ComponenteForm className="col-lg-6">
             <Label for="idnacimiento">Fecha de nacimiento</Label>
             <Input type="date" name="apellidos" id="idnacimiento" />
           </ComponenteForm>
           </Row>
           <Row>
           <ComponenteForm className="col-lg-12">
           <Label for="idfoto">Foto de Perfil</Label>
           <Input type="file" name="file" id="idfoto"/>
           <Button>Submit</Button>
           </ComponenteForm>
           </Row>
         </Form>
       </FondoRegistro>
   */

   /*
         <FondoRegistro className="container">
    <div><h1>Creando publicacion viajera</h1></div>
        <Form>
          <Row>
            <ComponenteForm className="col-lg-6">
            <FormGroup>
              <Label for="idnombre">Nombre</Label>
              <Input className="Input," type="text" name="nombre" id="idnombre" value="Manuela" disabled="disabled" />
              </FormGroup>
            </ComponenteForm>
            <ComponenteForm className="col-lg-6">
            <FormGroup>
              <Label for="idapellidos">Apellidos</Label>
              <Input className="Input," type="text" name="apellidos" id="idapellidos" value="La Viajera" disabled="disabled" />
              </FormGroup>
            </ComponenteForm>
          </Row>
          <Row>
            <ComponenteForm className="col-lg-6">
            <FormGroup>
              <Label for="fecha">Inicio del viaje</Label>
              <Input className="Input," type="date" name="iniviaje" id="fecha" />
              </FormGroup>
            </ComponenteForm>
            <ComponenteForm>
            <Col className="col-lg-12">
            <Label for="ididentificacion">Presupuesto</Label>
                 <Input className="Input," type="select" name="nombre" id="ididentificacion">
                   <option value="1">Opcion presupsueto bajo</option>
                   <option value="2">Opcion presupsueto medio</option>
                   <option value="3">Opcion presupsueto alto</option>
                 </Input></Col>
            </ComponenteForm>
          </Row>
          <Row>
            <ComponenteForm className="col-lg-12">
            <FormGroup>
              <Label for="exampleText">Text Area</Label>
              <Input className="Input," type="textarea" name="text" id="exampleText" />
              </FormGroup>
            </ComponenteForm>
          </Row>
          <Button>Enviar</Button>
        </Form>
      </FondoRegistro>
       */

   /* <ComponenteForm className="col-lg-6">
             <Label for="idemail">Email</Label>
             <Input type="email" name="email" id="idemail" value="ManuelaLaViajera@gmail.com" disabled="disabled"/>
           </ComponenteForm>
    */

  //  presupuesto fecha y descripcion

  return (
    <div className="App">
       <FondoRegistro className="container">
       <div><h1>Creando Publicacion Anfritriona</h1></div>
       </FondoRegistro>

      ciudad y descripcion id pais id usuario



    </div>
  );
}

export default App;
